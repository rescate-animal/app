(function () {
  angular.module('app.core')
    .run(runBlock);

  /* @ngInject */
  function runBlock($http, $templateCache, globalIcons, globalImages) {

    // Pre-fetch icons sources by URL and cache in the $templateCache...
    // subsequent $http calls will look there first.
    angular.forEach(globalIcons, function (value) {
      $http.get(value, {cache: $templateCache});
    });

    angular.forEach(globalImages, function (value) {
      $http.get(value, {cache: $templateCache});
    });

  }

})();
