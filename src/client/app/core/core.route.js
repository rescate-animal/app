(function () {
  'use strict';

  angular.module('app').config(states);

  /* @ngInject */
  function states($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/main');

    $stateProvider
      .state('404', {
        url: '/404',
        templateUrl: 'app/core/404.html'
      });

  }
})();
