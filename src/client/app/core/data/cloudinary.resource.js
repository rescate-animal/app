/* jshint -W106, -W117 */
(function () {
  'use strict';

  angular.module('app.features').factory('cloudinaryResource', cloudinaryResource);

  /* @ngInject */
  function cloudinaryResource($resource, $http, $q) {

    var url = $.cloudinary.url('myphotoalbum', {format: 'json', type: 'list'});
    url = url + '?' + Math.ceil(new Date().getTime() / 1000);

    function getPictures() {
      return $http.get(url)
        .then(sendPicturesResponse)
        .catch(sendGetPicturesError);
    }

    function getPicturesByAlbum(album) {

      var url = buildUrlByAlbum(album);

      return $http.get(url)
        .then(sendPicturesResponse)
        .catch(sendGetPicturesError);
    }

    function sendPicturesResponse(response) {
      return response.data;
    }

    function sendGetPicturesError(response) {
      var error;
      error = {
        message: 'Error retrieving pictures. ' +
        '(HTTP status: ' + response.status + ')',
        status: response.status
      };
      return $q.reject(error);
    }

    // util
    function buildUrlByAlbum(album) {
      var url = $.cloudinary.url(album, {format: 'json', type: 'list'});
      url = url + '?' + Math.ceil(new Date().getTime() / 1000);
      console.log('URL: ', url);
      return url;
    }

    var resource;

    resource = {
      getPictures: getPictures,
      getPicturesByAlbum: getPicturesByAlbum
    };

    return resource;

  }

})();

// REMEMBER

//$http({
//  method: 'GET',
//  url: url
//}).success(function (data, status, headers, config) {
//
//}).error(function (data, status, headers, config) {
//
//});
