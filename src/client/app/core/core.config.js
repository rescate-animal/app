(function () {
  angular.module('app.core')
    .config(configuration);

  /* @ngInject */
  function configuration($mdIconProvider, $mdThemingProvider, globalIcons, globalSizes) {
    $mdIconProvider
      .defaultIconSet(globalIcons.CORE, globalSizes.CORE_SIZE);

    //red
    //pink
    //purple
    //deep-purple
    //indigo
    //blue
    //light-blue
    //cyan
    //teal
    //green
    //light-green
    //lime
    //yellow
    //amber
    //orange
    //deep-orange
    //brown
    //grey
    //blue-grey

    $mdThemingProvider.theme('default')
      .primaryPalette('indigo')
      .accentPalette('pink')
      .warnPalette('red')
      .backgroundPalette('grey');
  }

})();
