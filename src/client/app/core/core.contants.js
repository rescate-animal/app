(function () {
  angular.module('app.core')
    .constant('globalIcons', globalIcons())
    .constant('globalLabels', globalLabels())
    .constant('globalStates', globalStates())
    .constant('globalSizes', globalSizes())
    .constant('globalImages', globalImages())
    .constant('globalKeys', globalKeys())
    .constant('globalAlbums', globalAlbums());

  function globalIcons() {
    var svg = 'images/svg/production/';
    var icons =
    {
      CORE: svg + 'core-icons.svg',
      FACE: svg + 'ic_face_24px.svg',
      ID: svg + 'ic_account_box_24px.svg',
      SEARCH: svg + 'ic_search_24px.svg'
    };
    return icons;
  }

  function globalLabels() {
    var lables = {
      DATE_BIRTH: 'Fecha de nacimiento',
      DRAG_DROP: 'Arrastre las imágenes que desea agregar :)',
      HEADER: 'Rescate Animal',
      ID: 'Cédula',
      NAME: 'Nombre',
      PRIMARY_LAST_NAME: 'Primer apellido',
      SECOND_LAST_NAME: 'Segundo apellido'
    };
    return lables;
  }

  function globalStates() {
    var states = {
      MAIN: {
        state: 'main',
        url: '/main',
        templateUrl: 'app/layout/main/main.html'
      }
    };
    return states;
  }

  function globalSizes() {
    var sizes = {
      CORE_SIZE: 24
    };
    return sizes;
  }

  function globalImages() {
    var root = 'images/';
    var images =
    {
      MAIN: root + 'main.jpg',
      GALLERY: 'images/svg/production/ic_account_box_24px.svg'
    };
    return images;
  }

  function globalKeys() {
    var keys = {};
    return keys;
  }

  function globalAlbums() {
    var albums = {
      DEFAULT: 'pets' // myphotoalbum
    };
    return albums;
  }

})();
