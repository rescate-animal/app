(function () {
  'use strict';
  angular.module('app.layout').config(states);

  /* @ngInject */
  function states($stateProvider) {

    $stateProvider

      .state('adoption', {
        url: '/adoption',
        templateUrl: 'app/layout/forms/adoption/adoption.html',
        controller: 'AdoptionController as adoptionController',
        abstract: true
      })
      .state('adoption.root', {
        views: {
          'rightInfo': {
            templateUrl: 'app/layout/forms/adoption/sideInfo/right.html',
            controller: 'RightSideInfoController as rightSideInfoController'
          },

          'leftInfo': {
            templateUrl: 'app/layout/forms/adoption/sideInfo/left.html'
          },
          '': {
            template: '<div ui-view></div>'
          }
        }
      })
      .state('adoption.root.preferences', {
        url: '/preferences',
        template: 'Preferences'
      })
      .state('adoption.root.mainInfo', {
        url: '/mainInfo',
        templateUrl: 'app/layout/forms/adoption/steps/mainInfo.html'
      });
  }

})();
