(function () {
  'use strict';

  angular
    .module('app')
    .controller('LeftSideInfoController', LeftSideInfoController);

  /* @ngInject */
  function LeftSideInfoController() {
    /* jshint validthis: true */
    var vm = this;

    vm.activate = activate;
    vm.name = 'Left Controller';
    activate();

    ////////////////

    function activate() {
    }

  }
})();
