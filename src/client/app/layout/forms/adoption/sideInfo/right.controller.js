(function () {
  'use strict';

  angular
    .module('app')
    .controller('RightSideInfoController', RightSideInfoController);

  /* @ngInject */
  function RightSideInfoController($scope) {
    /* jshint validthis: true */
    var vm = this;

    vm.activate = activate;
    vm.name = 'Right Controller';
    activate();

    ////////////////

    function activate() {
      updateCanvas();
    }

    window.addEventListener('resize', resizeCanvas, false);
    window.addEventListener('orientationchange', resizeCanvas, false);

    function updateCanvas() {
      vm.height = 0.8 * window.document.body.offsetHeight;
      vm.width = window.document.getElementById('adoption-right-wall').offsetWidth;
    }

    function resizeCanvas() {
      updateCanvas();
      console.log('Resize');
      console.log(vm.width, vm.height);
      $scope.$apply();
    }

  }
})();
