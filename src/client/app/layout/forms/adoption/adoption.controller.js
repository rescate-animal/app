(function () {
  'use strict';

  angular
    .module('app')
    .controller('AdoptionController', AdoptionController);

  /* @ngInject */
  function AdoptionController(globalIcons, globalLabels, globalStates) {
    /* jshint validthis: true */
    var vm = this;

    vm.activate = activate;
    vm.icons = globalIcons;
    vm.labels = globalLabels;
    vm.isCurrentFormCorrect = isCurrentFormCorrect;
    vm.nextStage = '.questions';
    activate();

    ////////////////

    function activate() {
    }

    function isCurrentFormCorrect() {
      return false;
    }

  }
})();
