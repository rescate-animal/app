(function () {
  'use strict';

  angular
    .module('app.layout')
    .controller('SidenavController', SidenavController);

  /* @ngInject */
  function SidenavController(globalLabels) {
    /* jshint validthis: true */
    var vm = this;
    vm.title = 'SidenavController';

    vm.modelMenu = {};

    vm.lables = globalLabels;

    //var sidenav = angular.element(document.querySelector('#md-sidenav-left'));

  }
})();
