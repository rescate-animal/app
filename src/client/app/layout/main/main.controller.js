(function () {
  'use strict';

  angular
    .module('app')
    .controller('MainController', MainController);

  /* @ngInject */
  function MainController($scope, $timeout, $mdSidenav, $mdUtil, $log, globalLabels) {
    /* jshint validthis: true */
    var vm = this;

    vm.isEnable = false;
    vm.activate = activate;
    vm.toggleSidenav = toggleSidenav;
    vm.closeSidenav = closeSidenav;
    vm.labels = globalLabels;

    function toggleSidenav(menuId) {
      // Open the given sidenav
      $mdSidenav(menuId).toggle()
        .then(function () {
          console.log('open ' + menuId + ' is done');
        });
    }

    function closeSidenav(menuId) {
      $mdSidenav(menuId).close()
        .then(function () {
          console.log('close ' + menuId + ' is done');
        });
    }

    ////////////////

    function activate() {
      showContent();
    }

    function showContent() {
      //Force a 1 second delay so we can see the splash.
      $timeout(function () {
        vm.isEnable = true;
      }, 1000);
    }

    activate();

  }
})();
