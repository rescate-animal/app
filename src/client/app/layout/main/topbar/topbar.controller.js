(function () {
  'use strict';

  angular
    .module('app.layout')
    .controller('TopbarController', TopbarController);

  /* @ngInject */
  function TopbarController(globalLabels) {
    /* jshint validthis: true */
    var vm = this;
    vm.labels = globalLabels;

    activate();

    ////////////////

    function activate() {

    }
  }
})();
