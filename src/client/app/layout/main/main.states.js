(function () {
  'use strict';
  angular.module('app.layout').config(states);

  /* @ngInject */
  function states($stateProvider, $urlRouterProvider, globalStates) {

    $urlRouterProvider.otherwise(globalStates.MAIN.url);

    $stateProvider

      .state(globalStates.MAIN.state, {
        url: globalStates.MAIN.url,
        templateUrl: globalStates.MAIN.templateUrl,
        controller: 'MainController as mainController'
      });

  }

})();
