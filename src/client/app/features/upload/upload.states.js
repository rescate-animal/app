(function () {
  'use strict';
  angular.module('app.features').config(states);

  /* @ngInject */
  function states($stateProvider, globalStates) {

    $stateProvider

      .state('upload', {
        url: '/upload',
        templateUrl: 'app/features/upload/upload.html',
        controller: 'UploadController as uploadController'
      });

  }

})();
