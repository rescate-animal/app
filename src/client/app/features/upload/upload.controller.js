/* jshint -W106, -W117 */
/*jshint camelcase: false */
(function () {
  'use strict';

  angular
    .module('app.features')
    .controller('UploadController', UploadController);

  /* @ngInject */
  function UploadController($scope, $rootScope, Upload, $mdToast, $timeout, globalLabels,
                            globalAlbums) {
    /* jshint validthis: true */
    var vm = this;

    vm.upload = upload;
    vm.mode = 'buffer';
    vm.fileList = [];
    vm.iconUpload = 'cloud_upload';
    vm.uploadReady = false;
    vm.labels = globalLabels;

    activate();
    ////////////////

    // TODO: make improve perform, remove use $watch
    $scope.$watch('uploadController.files', function (current, original) {
      if (current) {
        vm.upload(current);
        console.log('great', current);
      }
    });

    function upload(files) {
      if (!files) {
        return;
      }

      vm.fileList = files;

      files.forEach(function (file) {
        file.before = file.after = 0;
        vm.iconUpload = 'cloud_upload';
        vm.dragover = 'dragover';
        Upload.upload({
          url: 'https://api.cloudinary.com/v1_1/' + 'rb7373' + '/upload',
          data: {
            tags: globalAlbums.DEFAULT,
            context: 'photo='
          },
          file: file,
          fields: {
            upload_preset: 'rgwg1xvw',
            tags: globalAlbums.DEFAULT,
            context: 'photo='
          }
        }).progress(function (event) {
          file.before = file.after;
          file.after = Math.round((event.loaded * 100.0) / event.total);
          console.log('progress', file.after);
          file.status = 'Uploading... ' + file.after + '%';
          if (!$scope.$$phase) {
            $scope.$apply();
          }
        }).success(function (data, status, headers, config) {
          file.result = data;
          vm.iconUpload = 'cloud_done';
          vm.dragover = '';
          if (!$scope.$$phase) {
            $scope.$apply();
          }
          file.uploadReady = true;
          $mdToast.show(
            $mdToast.simple()
              .content('Carga completa!')
              .position('bottom right')
              .hideDelay(3000)
          );
          file.finished = true;
          //Force a 1 second delay so we can see the splash.
          $timeout(function () {
            vm.iconUpload = 'cloud_upload';
          }, 2000);
        });
      });
    }

    //////////////////

    function activate() {
      $.cloudinary.config().cloud_name = 'rb7373';
      $.cloudinary.config().upload_preset = 'rgwg1xvw';
      $.cloudinary.config({cloud_name: 'rb7373', api_key: '913833548897647'});
    }

  }
})();
