/* jshint -W106, -W117 */
(function () {
  'use strict';

  angular
    .module('app.layout')
    .factory('searchGalleryModel', searchGalleryModel);

  /* @ngInject */
  function searchGalleryModel($q, globalImages) {
    /* jshint validthis:true */
    var readyPromise;

    var height;

    var imageList = [
      {
        name: 'Cadmium',
        symbol: 'Cd',
        number: '48',
        weight: '112.411',
        css: 'transition metal',
        category: 'transition',
        image: globalImages.GALLERY,
        height: '100',
        width: '200',
        color: 'blue'
      },
      {
        name: 'Calcium',
        symbol: 'Ca',
        number: '20',
        weight: '40.078',
        css: 'alkaline-earth metal',
        category: 'alkaline-earth',
        image: globalImages.GALLERY,
        height: '200',
        width: '200',
        color: 'red'
      },
      {
        name: 'Rhenium',
        symbol: 'Re',
        number: '75',
        weight: '186.207',
        css: 'transition metal',
        category: 'transition',
        image: globalImages.GALLERY,
        height: '70',
        width: '80',
        color: 'yellow'
      }
    ];

    var types = [
      'post-transition metal',
      'transition metal',
      'metalloid',
      'alkali metal',
      'alkaline-earth metal',
      'lanthanoid metal inner-transition',
      'noble-gas nonmetal'
    ];

    function configureCloudinary() {
      $.cloudinary.config({cloud_name: 'rb7373', api_key: '913833548897647'});
    }

    configureCloudinary();

    ///////////////////////

    var service = {
      getImages: getImages,
      getTypes: getTypes
    };

    return service;

    ////////////////////////

    function getImages() {
      return $q.when(imageList);
    }

    function getTypes() {
      return $q.when(types);
    }

  }
})();
