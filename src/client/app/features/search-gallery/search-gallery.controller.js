(function () {
  'use strict';

  angular
    .module('app.features')
    .controller('SearchGalleryController', SearchGalleryController);

  /* @ngInject */
  function SearchGalleryController(searchGalleryModel, globalImages, cloudinaryResource,
                                   globalAlbums, $mdToast) {
    /* jshint validthis: true */
    var vm = this;

    vm.activate = activate;
    vm.add = add;
    vm.reset = reset;
    vm.clear = clear;

    vm.imageList = [];
    vm.types = [];
    vm.pictures = {};
    vm.swift = swift;
    vm.getColor = getColor;

    function getColor() {
      return Math.floor(Math.random() * 16777215).toString(16);
    }

    ////////////////

    function activate() {
      getPicturesByAlbum(globalAlbums.DEFAULT).then(function () {
        console.log('Activated SearchGallery View: getPicturesByAlbum');
      });
      updateImages();
      //getTypes();
      //swift();
    }

    function updateImages() {
      searchGalleryModel.getImages().then(function (result) {
        vm.imageList = angular.copy(result);
        for (var i = 0, max = 50; i < max; ++i) {
          add();
        }
        console.log(vm.imageList);
      });
    }

    function getTypes() {
      searchGalleryModel.getTypes().then(function (result) {
        vm.types = angular.copy(result);
        console.log(vm.types);
      });
    }

    function add() {
      var css = vm.types[Math.floor(Math.random() * vm.types.length)];
      var newElement = {
        name: Math.random().toString(36).substring(7),
        symbol: Math.random().toString(36).substring(16),
        number: '95',
        weight: (Math.random() * 10).toFixed(4),
        css: css,
        category: 'lanthanoid',
        image: globalImages.GALLERY,
        height: (Math.random() * 300).toFixed(4),
        width: (Math.random() * 300).toFixed(4)
      };
      vm.imageList.push(newElement);
      console.log('add');
    }

    function clear() {
      vm.imageList = [];
      console.log('clear');
    }

    function reset() {
      updateImages();
      console.log('reset');
    }

    var currentLayoutIndex = 0;
    var layouts = ['masonry', 'fitRows', 'vertical', 'packery',
      'masonryHorizontal', 'fitColumns'];

    function swift() {
      currentLayoutIndex = currentLayoutIndex === layouts.length - 1 ? 0 : currentLayoutIndex + 1;
      vm.currentLayout = layouts[currentLayoutIndex];
      console.log(vm.currentLayout);
    }

    function getPicturesByAlbum(album) {
      return cloudinaryResource.getPicturesByAlbum(album)
        .then(onPicturesSuccess, null, onPicturesNotification)
        .catch(onError)
        .finally(onGetPicturesComplete);
    }

    function onPicturesSuccess(pictures) {
      vm.pictures = pictures.resources;
      console.log('Pictures from server: ');
      console.log(pictures);
    }

    function onGetPicturesComplete() {
      console.log('Pictures from server complete');
    }

    function onPicturesNotification(notification) {
      console.log('Promise notification: ' + notification);
    }

    function onError(error) {
      console.log('Error message: ' + error.message);
      console.log('Error code: ' + error.status);

      vm.pictures = null;
      showErrorNotFound();
    }

    function showErrorNotFound() {
      $mdToast.show(
        $mdToast.simple()
          .content('Sin resultados :(!')
          .position('top right')
          .hideDelay(3000)
      );
    }

    /////////////// Calls

    activate();

  }
})();
