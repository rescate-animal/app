(function () {
  'use strict';
  angular.module('app.layout').config(states);

  /* @ngInject */
  function states($stateProvider) {

    $stateProvider

      .state('gallery', {
        url: '/gallery',
        templateUrl: 'app/features/search-gallery/search-gallery.html',
        controller: 'SearchGalleryController as searchGalleryController',
        abstract: true
      })
      .state('gallery.root', {
        views: {
          'rightInfo': {
            template: 'right'
          },

          'leftInfo': {
            template: 'left'
          },
          '': {
            template: '<div ui-view></div>'
          }
        }
      })
      .state('gallery.root.mainInfo', {
        url: '/mainInfo',
        templateUrl: 'app/features/search-gallery/main/main.html'
      });
  }

})();
